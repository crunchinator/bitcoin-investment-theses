Bitcoin Investment Cases of several organizations. Kudos to 100TrillionUSD for the initial gathering; this will be my own copy where I add more texts that I come across.

* [Grayscale](Grayscale - Valuing Bitcoin.pdf)
* [BVI](BVI Letter - Macro Outlook [May2020].pdf)
* [Microstrategy](MicroStrategy Adopts Bitcoin as Primary Treasury Reserve Asset.pdf)
* [Fidelity - Bitcoin as an Aspirational Store of Value](Fidelity - Bitcoin - An Aspirational Store of Value.pdf)
* [Fidelity - Bitcoin's Role as an Alternative Investment](Fidelity - Bitcoin - Role as an Alternative Investment.pdf)
* [VanEck](VanEck - The Investment Case for Bitcoin.pdf)
* [Square, Inc.](Square,%20Inc.%20-%20Bitcoin%20Investment%20Whitepaper.pdf)
